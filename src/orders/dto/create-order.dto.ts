import { IsNotEmpty, MinLength } from 'class-validator';
class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  orderItems: CreatedOrderItemDto[];
}
